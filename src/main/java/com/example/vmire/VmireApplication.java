package com.example.vmire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.example.vmire")
public class VmireApplication {

	public static void main(String[] args) {
		SpringApplication.run(VmireApplication.class, args);
	}

}
