package com.example.vmire.controller;

import com.example.vmire.model.Menu;
import com.example.vmire.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

@RestController
public class MenuController {
    @Autowired
    private MenuRepository menuRepository;

    @PostMapping("/menu/seed")
    private ResponseEntity seedMenu() {
        ArrayList<Menu> menus = new ArrayList<>();

        for(int i = 1; i <= 10 ; i++) {
            String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
            String randomName = "";

            for(int j = 1; j < 11; j++) {
                Random random = new Random();
                randomName = randomName.concat(String.valueOf(((char) (random.nextInt(26) + 'A'))));
            }

            Menu menu = new Menu();
            menu.setId(id);
            menu.setName(randomName);
            menu.setPrice(10);

            menuRepository.save(menu);
            menus.add(menu);
        }

        return ResponseEntity.ok(menus);
    }
}
