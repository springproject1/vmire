package com.example.vmire.controller;

import com.example.vmire.model.Menu;
import com.example.vmire.model.TransactionDetail;
import com.example.vmire.model.TransactionHeader;
import com.example.vmire.repository.MenuRepository;
import com.example.vmire.repository.TransactionDetailRepository;
import com.example.vmire.repository.TransactionHeaderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.*;

@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private TransactionHeaderRepository transactionHeaderRepository;

    @Autowired
    private TransactionDetailRepository transactionDetailRepository;

    /*
    Route::get('/transaction', 'TransactionController@getAll')

        Nama fungsi     : getAll
        Deskripsi       : Fungsi digunakan untuk mengambil semua data transaksi
        Developer       : Mijuha
        Version History :
            Ver     Short Desc                      Developer
            1.0     Initial Function                Mijuha
            1.1     Refactor transaction selection  Johan
    */
    @GetMapping("")
    private ResponseEntity getAll() {
        ArrayList<TransactionHeader> transactionHeaders = (ArrayList<TransactionHeader>) transactionHeaderRepository.findAll(); //sama seperti transactionHeader->all()

        return ResponseEntity.ok(transactionHeaders);
    }

    /*
    Route::get('/transaction/{transactionId}', 'TransactionController@getTransactionById')

        Nama fungsi     : getTransactionById
        Deskripsi       : Fungsi digunakan untuk mengambil detail data transaksi by Id
        Developer       : Mijuha
        Version History :
            Ver     Short Desc                      Developer
            1.0     Initial Function                Mijuha
            1.1     Refactor transaction selection  Johan
    */
    @GetMapping("/{transactionId}")
    private ResponseEntity getTransactionById(@PathVariable String transactionId) {
        // Flow nya : Get transaction detail by Id from DB, calculate price, return

        List<TransactionDetail> transactionDetails = transactionDetailRepository.findTransactionDetailsByTransactionId(transactionId);

        List<HashMap<String, Object>> result = new ArrayList<>(); //Sama seperti new Object

        int totalPrice = 0;

        for(TransactionDetail detail : transactionDetails) {
            Menu menu = menuRepository.findMenuById(detail.getMenuId());
            int price = menu.getPrice() * detail.getQty();

            HashMap<String, Object> calculated = new HashMap<>();
            calculated.put("menuName", menu.getName());
            calculated.put("totalPrice", price);
            calculated.put("qty", detail.getQty());

            result.add(calculated);

            /*
                Bentuk diatas jika diubah ke php bisa seperti ini:
                $price = $menu->price * $menu->qty;
                Object $calculated = new Object();
                $calculated->menuName = $menu->name;
                $calculated->totalPrice = $price;
                $calulated->qty = $menu->qty;

                $array->push($calculated); Bisa baca ke dokumentasi php
             */

            totalPrice += price; // Sama seperti totalPrice = totalPrice + price
        }

        HashMap<String, Object> response = new HashMap<>();
        response.put("total", totalPrice);
        response.put("orders", result);

        return ResponseEntity.ok(response);

        /*
                Bentuk diatas jika diubah ke php bisa seperti ini:
                Object $response = new Object();
                $response->total = $totalPrice;
                $response->orders = $array;

                return view('...', ['order' => $response])
             */

    }

    //Route::post('/transaction/{id}/pay', 'TransactionController@checkout')
    //Route::put('/transaction/{id}', 'TransactionController@update')
    //Route::delete('/transaction/{id}', 'TransactionController@deleteTransaction')

    //Route::post('/transaction/create', 'TransactionController@makeTransaction')

    /*
    Route::get('/transaction', 'TransactionController@getAll')
        Deskripsi       : Fungsi digunakan untuk membuat sebuah transaksi dan input kedalam db
     */

    @PostMapping("/create")
    private ResponseEntity makeTransaction(@RequestBody HashMap<String, Object> transactionRequest) {
        String transactionId = UUID.randomUUID().toString().toUpperCase().replace("-", ""); //Bikin transaction ID

        //Setelah diterima controller, save ke DB

        List<HashMap<String, Object>> menus = (List<HashMap<String, Object>>) transactionRequest.get("orders");

        // Save transactionHeader
        TransactionHeader header = new TransactionHeader();
        header.setTransactionId(transactionId);
        header.setIsFinished(false);
        header.setCreatedAt(new Timestamp((new Date()).getTime()));

        header = transactionHeaderRepository.save(header);

        // Save transactionDetail, caranya looping sejumlah order di request, request adalah data yg dikirim dari halaman view
        for(HashMap<String, Object> menu : menus) {
            System.out.println(menu);
            TransactionDetail detail = new TransactionDetail();
            detail.setTransactionId(header.getTransactionId());
            detail.setMenuId(menu.get("menuId").toString());
            detail.setQty(Integer.parseInt(menu.get("qty").toString()));
            detail.setItemId(UUID.randomUUID().toString());

            transactionDetailRepository.save(detail);
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(header); //Bisa dibuat return redirect atau return view
    }


}
