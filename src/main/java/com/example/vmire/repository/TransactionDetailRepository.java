package com.example.vmire.repository;

import com.example.vmire.model.TransactionDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionDetailRepository extends JpaRepository<TransactionDetail, String> {
    List<TransactionDetail> findTransactionDetailsByTransactionId(String transactionId);
}
