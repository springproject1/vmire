package com.example.vmire.repository;

import com.example.vmire.model.TransactionHeader;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionHeaderRepository extends JpaRepository<TransactionHeader, String> {
}
