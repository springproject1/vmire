package com.example.vmire.repository;

import com.example.vmire.model.Menu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuRepository extends JpaRepository<Menu, String> {
    Menu findMenuById(String id);
}
