package com.example.vmire.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "transaction_details")
public class TransactionDetail {
    @Column(name = "transaction_id")
    private String transactionId;

    @Id
    @Column(name = "item_id")
    private String itemId;

    @Column(name = "menu_id")
    private String menuId;

    @Column(name = "qty")
    private Integer qty;
}
