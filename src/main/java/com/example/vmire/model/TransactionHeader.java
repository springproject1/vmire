package com.example.vmire.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "transaction_headers")
@ToString
public class TransactionHeader {
    @Id
    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "is_finished")
    private Boolean isFinished;

    @Column(name = "created_at")
    private Timestamp createdAt;
}
