package com.example.vmire.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "menus")
public class Menu {
    @Id
    @Column(name = "menu_id")
    private String id;

    @Column(name = "menu_name")
    private String name;

    @Column(name = "menu_price")
    private Integer price;
}
